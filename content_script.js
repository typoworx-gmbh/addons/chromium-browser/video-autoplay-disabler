class MediaElementObserver {
  constructor() {
    if (this.isValidUrl(window.location.href)) {
      this.mediaElementHandler();
      document.addEventListener('DOMContentLoaded', () => this.mediaElementHandler());
    }
  }

  mediaElementHandler() {
    document.querySelectorAll('video, audio').forEach((mediaElement) => {
      this.disableAutoplayAndMute(mediaElement);
    });
  }

  disableAutoplayAndMute(mediaElement) {
    mediaElement.muted = true;
    mediaElement.pause();
    mediaElement.removeAttribute('autoplay');
    mediaElement.setAttribute('preload', 'metadata');

    mediaElement.addEventListener('play', () => {
      mediaElement.pause();
      mediaElement.muted = false;
      this.startTracking(mediaElement);
    }, { once: true });

    mediaElement.addEventListener('play', () => {
      this.startTracking(mediaElement);
    });

    mediaElement.addEventListener('pause', () => {
      this.stopTracking();
    });

    mediaElement.addEventListener('ended', () => {
      this.stopTracking();
    });
  }

  observeMutations() {
    const observer = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        mutation.addedNodes.forEach((node) => {
          if (node.nodeType === 1 && (node.tagName === 'VIDEO' || node.tagName === 'AUDIO')) {
            this.disableAutoplayAndMute(node);
          }
        });
      });
    });

    observer.observe(document.body, { childList: true, subtree: true });
  }

  startTracking(mediaElement) {
    const updateURL = () => {
      if (!mediaElement.paused && !mediaElement.ended) {
        const currentTime = Math.floor(mediaElement.currentTime);
        const url = new URL(window.location.href);
        url.searchParams.set('t', currentTime);
        window.history.replaceState({}, '', url);
        
        console.log('Video Time Tracker', currentTime);
      }
    };
    
    this.trackTimeout = setTimeout(updateURL, 5000); // 20 seconds
    updateURL();
  }

  stopTracking() {
    clearTimeout(this.trackTimeout);
  }

  isValidUrl(url) {
    const restrictedProtocols = ['chrome://', 'chrome-extension://', 'about:', 'view-source:', 'file://'];
    return !restrictedProtocols.some(protocol => url.startsWith(protocol));
  }
}

new MediaElementObserver();

