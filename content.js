document.querySelectorAll('video, audio').forEach((mediaElement) => {
  console.debug('Stopping Audio/Video');
  
  mediaElement.removeAttribute('autoplay');
  mediaElement.pause();
  mediaElement.addEventListener('play', () => {
    mediaElement.pause();
  }, true);
});

