chrome.runtime.onInstalled.addListener(() => {
  console.log('Disable HTML5 Autoplay extension installed');
});

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  if (changeInfo.status === 'complete' && tab.url && isValidUrl(tab.url)) {
    chrome.scripting.executeScript({
      target: { tabId: tabId, allFrames: true },
      files: ['content_script.js']
    }).catch(err => {
      if (err.message.includes('Cannot access contents of url')) {
        console.warn('Suppressed error: ', err.message);
      } else {
        console.error(err);
      }
    });
  }
});

function isValidUrl(url) {
  const restrictedProtocols = ['chrome://', 'chrome-extension://', 'about:', 'view-source:', 'file://'];

  return !restrictedProtocols.some(protocol => url.startsWith(protocol));
}

